/*
 *    My very own license
 */
package io.cdap.plugin.gcp.common;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import javax.annotation.Nullable;

import com.google.api.gax.grpc.InstantiatingGrpcChannelProvider;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.cloud.pubsub.v1.TopicAdminSettings;

import io.grpc.HttpConnectProxiedSocketAddress;
import io.grpc.ProxiedSocketAddress;
import io.grpc.ProxyDetector;

public class ProxyPatcher {
  private static String PROXY_HTTP_HOST_NAME = "http.proxyHost";
  private static String PROXY_HTTP_PORT_NAME = "http.proxyPort";
  private static String PROXY_HTTPS_HOST_NAME = "https.proxyHost";
  private static String PROXY_HTTPS_PORT_NAME = "https.proxyPort";


  /**
   * Configuraring Sub client for proxy
   *
   * Proxy priority HTTPS -> HTTP -> No Proxy
   */
  public static void configureChannelProxy(TopicAdminSettings.Builder builder) throws NumberFormatException {
    String myProxyHost = null;
    int myProxyPort = 0;

    if (System.getProperties().containsKey(PROXY_HTTPS_HOST_NAME)
      && System.getProperties().containsKey(PROXY_HTTPS_PORT_NAME)) {
      myProxyHost = System.getProperty(PROXY_HTTPS_HOST_NAME);
      myProxyPort = Integer.parseInt(System.getProperty(PROXY_HTTPS_PORT_NAME));
    } else if (System.getProperties().containsKey(PROXY_HTTP_HOST_NAME)
      && System.getProperties().containsKey(PROXY_HTTP_PORT_NAME)) {
      myProxyHost = System.getProperty(PROXY_HTTP_HOST_NAME);
      myProxyPort = Integer.parseInt(System.getProperty(PROXY_HTTP_PORT_NAME));
    }

    if (myProxyHost != null && myProxyPort != 0) {
      SocketAddress paroxySocketAddress = new InetSocketAddress(myProxyHost, myProxyPort);
      builder.setTransportChannelProvider(
        InstantiatingGrpcChannelProvider.newBuilder()
          .setChannelConfigurator(managedChannelBuilder -> managedChannelBuilder.proxyDetector(new ProxyDetector() {
            @Nullable
            @Override
            public ProxiedSocketAddress proxyFor(SocketAddress socketAddress) throws IOException {
              if (socketAddress == null) return null;

              return HttpConnectProxiedSocketAddress.newBuilder()
                .setTargetAddress((InetSocketAddress) socketAddress)
                .setProxyAddress(paroxySocketAddress)
                .build();
            }
          }))
          .build()
      );
    }
  }

  /**
   * Configuraring Pub client for proxy
   *
   * Proxy priority HTTPS -> HTTP -> No Proxy
   */
  public static void configureChannelProxy(Publisher.Builder builder) throws NumberFormatException {
    String myProxyHost = null;
    int myProxyPort = 0;

    if (System.getProperties().containsKey(PROXY_HTTPS_HOST_NAME)
      && System.getProperties().containsKey(PROXY_HTTPS_PORT_NAME)) {
      myProxyHost = System.getProperty(PROXY_HTTPS_HOST_NAME);
      myProxyPort = Integer.parseInt(System.getProperty(PROXY_HTTPS_PORT_NAME));
    }

    if (System.getProperties().containsKey(PROXY_HTTP_HOST_NAME)
      && System.getProperties().containsKey(PROXY_HTTP_PORT_NAME)) {
      myProxyHost = System.getProperty(PROXY_HTTP_HOST_NAME);
      myProxyPort = Integer.parseInt(System.getProperty(PROXY_HTTP_PORT_NAME));
    }

    if (myProxyHost != null && myProxyPort != 0) {
      SocketAddress paroxySocketAddress = new InetSocketAddress(myProxyHost, myProxyPort);
      builder.setChannelProvider(
        InstantiatingGrpcChannelProvider.newBuilder()
          .setChannelConfigurator(managedChannelBuilder -> managedChannelBuilder.proxyDetector(new ProxyDetector() {
            @Nullable
            @Override
            public ProxiedSocketAddress proxyFor(SocketAddress socketAddress) throws IOException {
              if (socketAddress == null) return null;

              return HttpConnectProxiedSocketAddress.newBuilder()
                .setTargetAddress((InetSocketAddress) socketAddress)
                .setProxyAddress(paroxySocketAddress)
                .build();
            }
          }))
          .build()
      );
    }
  }
}
